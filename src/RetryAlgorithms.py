class SimpleRetryAlgorithm(object):
         
    def __init__(self,interval, timeToLive, maxRetry, lowerPriority):
        self.interval=interval
        self.timeToLive=timeToLive
        self.maxRetry=maxRetry
        self.lowerPriority=lowerPriority
    
    def getInterval(self):
        return self.interval
    
    def getMaxRetry(self):
        return self.maxRetry  
                      
    def getLowerPriority(self):
        return self.lowerPriority
    
    def getTimeToLive(self):
        return self.timeToLive
    
    def __str__(self):        
        return SimpleRetryAlgorithm.__name__+":[interval:"+str(self.interval)+",maxRetry:"+str(self.maxRetry)+",timeToLive:"+str(self.timeToLive)+",lowerPriority:"+str(self.lowerPriority)+"]"
    
class BackoffRetryAlgorithm(SimpleRetryAlgorithm):
    
    def __init__(self,interval, timeToLive, maxRetry, lowerPriority):
        self.interval=interval
        self.timeToLive=timeToLive
        self.maxRetry=maxRetry
        self.lowerPriority=lowerPriority

class JmsServerContext(object):
    
    def __init__(self,serverUrl,routerName,connectionFactory):
        self.serverUrl=serverUrl
        self.routerName=routerName
        self.connectionFactory=connectionFactory
    
    def getServerUrl(self):
        return self.serverUrl
    
    def getRouterName(self):
        return self.routerName
    
    def getConnectionFactory(self):
        return self.connectionFactory
    
class MessageEventProducer(object):
    
    def __init__(self, serverContext, queueName, bulkDeliveryEventQueueName):
        self.serverContext=serverContext
        self.queueName=queueName
        self.bulkDeliveryEventQueueName=bulkDeliveryEventQueueName
        
    def getServerContext(self):
        return self.serverContext
    
    def getQueueName(self):
        return self.queueName
    
    def getBulkDeliveryEventQueueName(self):
        return self.bulkDeliveryEventQueueName

class RerouteAlgorithm(object):
    
    def __init__(self, rules,eventProducer,markAsBilledIfSubmitted):
        self.rules=rules      
        self.eventProducer=eventProducer
        self.markAsBilledIfSubmitted=markAsBilledIfSubmitted  
    
    def getRules(self):
        return self.rules
    
    def getEventProducer(self):
        return self.eventProducer
    
    def isMarkAsBilledIfSubmitted(self):
        return self.isMarkAsBilledIfSubmitted()

class ShortcodeRebillAlgorithm(object):

    def __init__(self, shortCodeAttempts, lowerPriority):
        self.shortCodeAttempts=shortCodeAttempts        
        self.lowerPriority=lowerPriority    
    
    def getShortCodeAttempts(self):
        return self.shortCodeAttempts

    def getLowerPriority(self):
        return self.lowerPriority
    
class ShortcodeOptOutAlgorithm(object):
    
    def __init__(self, eventProducer, produceEvent):
        self.eventProducer=eventProducer
        self.produceEvent=produceEvent
        
    def getEventProducer(self):
        return self.eventProducer
    
    def isProduceEvent(self):
        return self.produceEvent
                