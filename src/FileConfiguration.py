from RetryAlgorithms import *

class FileConfiguration(object):
        
    def __init__(self, fileName):
        self.mappings={ShortcodeOptOutAlgorithm.__name__:[],SimpleRetryAlgorithm.__name__:[],ShortcodeRebillAlgorithm.__name__:[],RerouteAlgorithm.__name__:[],BackoffRetryAlgorithm.__name__:[]}
        self.fileName=fileName 
        
    def addMapping(self, foo):
        self.mappings[foo.getAlgorithm().__class__.__name__].append(foo)    
    
    def getSimpleRetryAlgorithmMappings(self):
        return self.mappings[SimpleRetryAlgorithm.__name__]
        
    def getShortcodeRebillAlgorithmsMappings(self):
        return self.mappings[ShortcodeRebillAlgorithm.__name__]
    
    def getRerouteAlgorithmAlgorithmMappings(self):
        return self.mappings[RerouteAlgorithm.__name__]
    
    def getBackoffRetryAlgorithmMappings(self):
        return self.mappings[BackoffRetryAlgorithm.__name__]
        
    def getShortcodeOptOutAlgorithmMappings(self):
        return self.mappings[ShortcodeOptOutAlgorithm.__name__]
    
    def getFileName(self):
        return self.fileName
        