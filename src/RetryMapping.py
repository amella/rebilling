class RetryMapping(object):
    
    def __init__(self,algorithm, errorList):
        self.algorithm=algorithm  
        self.errorList=errorList         
    
    def getAlgorithm(self):
        return self.algorithm
    
    def getErrorList(self):
        return self.errorList
    
    def __str__(self):
        return "RetryMapping:["+str(self.algorithm)+",errorList["+",".join(self.errorList)+"]]"