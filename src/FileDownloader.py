import paramiko
import os
import shutil
from Constants import *

class FileDownloader(object):
    
    def __init__(self, root, username, password, mode=0755):                
        self.root=root
        self.mode=mode        
        self.username=username
        self.password=password       
      
        self.createInitialDirectoryStructure()  
    
    def cleanUpDirs(self):
        
        for serverName in Constants.serverNames:
            
            path=os.path.join(self.root, serverName)
            
            if os.path.exists(path):
                
                shutil.rmtree(path)
    
    def createInitialDirectoryStructure(self):

        self.cleanUpDirs()
        
        for serverName in Constants.serverNames:
                   
            os.mkdir(os.path.join(self.root,serverName),self.mode)  
            os.mkdir(os.path.join(self.root,serverName,"opt"),self.mode)
            os.mkdir(os.path.join(self.root,serverName,"opt","wimax"),self.mode)
            os.mkdir(os.path.join(self.root,serverName,"opt", "wimax", "conf"),self.mode)     
       
    def downloadFiles(self):
                
        for index, serverIp in enumerate(Constants.serverIps):
        
            ssh=paramiko.SSHClient() 
            ssh.load_system_host_keys()
            
            ssh.connect(serverIp,username=self.username, password=self.password, compress=True)
    
            sftp=ssh.open_sftp()
    
            stdin, stdout, stderr = ssh.exec_command("ls /"+os.path.join("opt", "wimax", "conf")+" | grep -E '^[A-Za-z]{2}$'")
            stdin.close()
            
            for line in stderr.read().splitlines():    
                print line
    
            for i in stdout.read().splitlines():
                   
                os.mkdir(os.path.join(self.root,Constants.serverNames[index],"opt", "wimax", "conf",i),self.mode)     
                       
                stdin, stdout, stderr = ssh.exec_command("ls /"+os.path.join("opt", "wimax", "conf",i)+"/*.xml | grep -v -E '(_retry|_wimax|black|bulk|content|hibernate|jetty)'")
                stdin.close()
            
                for line in stderr.read().splitlines():    
                    print line
                      
                for line in stdout.read().splitlines():         
                    sftp.get(line,os.path.join(self.root,Constants.serverNames[index])+line)
    
            ssh.close()
        
        print "Done!"

    def startDownloadingFiles(self):

        self.createInitialDirectoryStructure()
        self.downloadFiles()

foo=FileDownloader("/home/andres","logviewer","%7|_-#7-k%8S}]")
foo.downloadFiles()
