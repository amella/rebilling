import libxml2
import os
from Util import joinStrings
from RetryAlgorithms import *
from RetryMapping import *
import sys
import FileConfiguration
from Constants import *
from time import strftime
import sys

class RebillingAnalyser(object):

    def __init__(self, root):
        self.root=root        

    def isShortcodeOptOutAlgorithm(self,ctx):
        try: 
        
            ctx.xpathEval("property/bean[@class='com.wi.max.transport.retry.algorithm.ShortcodeOptOutAlgorithm']")[0].getContent().strip()            
                        
            return True            
                                              
        except IndexError:
            return False

    def isBackoffRetryAlgorithm(self,ctx):
        try: 
        
            ctx.xpathEval("property/bean[@class='com.wi.max.transport.retry.algorithm.BackoffRetryAlgorithm']")[0].getContent().strip()            
                        
            return True            
                                              
        except IndexError:
            return False
        
    def isSimpleRetryAlgorithmMapping(self, ctx):
        try: 
        
            ctx.xpathEval("property/bean[@class='com.wi.max.transport.retry.algorithm.SimpleRetryAlgorithm']")[0].getContent().strip()            
                        
            return True            
                                              
        except IndexError:
            return False

    def isShortcodeRebillAlgorithmMapping(self, ctx):
        try: 
        
            ctx.xpathEval("property/bean[@class='com.wi.max.transport.retry.algorithm.ShortcodeRebillAlgorithm']")[0].getContent().strip()
                                                      
            return True            
                                              
        except IndexError:
            return False

    def isRerouteAlgorithmMapping(self, ctx):
        try: 
        
            ctx.xpathEval("property/bean[@class='com.wi.max.transport.retry.algorithm.RerouteAlgorithm']")[0].getContent().strip()
                                                      
            return True            
                                              
        except IndexError:
            return False
    """
    """
    def parseJmsServerContext(self, ctx):
        pass
    """
          <bean id="messageEventProducer" class="com.wi.max.event.MessageEventProducer">
        <constructor-arg index="0">
            <ref bean="jmsServerContext"/> <!-- server context -->
        </constructor-arg>
        <constructor-arg index="1">
            <value>message_delivery_event</value> <!-- default queue name -->
        </constructor-arg>
        <constructor-arg index="2">
            <value>bulk_delivery_event</value> <!-- bulk delivery event queue name, event w/o triggers -->
        </constructor-arg>
    </bean>           
        """            
    def parseMessageEventProducer(self, ctx,id):
        pass
    
    """
    """
    def parseShortcodeOptOutAlgorithm(self,ctx):
        pass

    def parseBackoffRetryAlgorithm(self,ctx):
        
        interval=ctx.xpathEval("property/bean/property[@name='interval']")[0].getContent().strip()
        maxRetry=ctx.xpathEval("property/bean/property[@name='maxRetry']")[0].getContent().strip()
        timeToLive=ctx.xpathEval("property/bean/property[@name='timeToLive']")[0].getContent().strip()
        lowerPriority=ctx.xpathEval("property/bean/property[@name='lowerPriority']")[0].getContent().strip()
                           
        algorithm=BackoffRetryAlgorithm(interval, timeToLive, maxRetry, lowerPriority)

        return algorithm

    def parseSimpleRetryAlgorithm(self,ctx):
        
        interval=ctx.xpathEval("property/bean/property[@name='interval']")[0].getContent().strip()
        maxRetry=ctx.xpathEval("property/bean/property[@name='maxRetry']")[0].getContent().strip()
        timeToLive=ctx.xpathEval("property/bean/property[@name='timeToLive']")[0].getContent().strip()
        lowerPriority=ctx.xpathEval("property/bean/property[@name='lowerPriority']")[0].getContent().strip()
                           
        algorithm=SimpleRetryAlgorithm(interval, timeToLive, maxRetry, lowerPriority)

        return algorithm
    
    def parseShortcodeRebillAlgorithm(self,ctx):
        
        lowerPriority=ctx.xpathEval("property/bean/property[@name='lowerPriority']")[0].getContent().strip()
        
        entries=ctx.xpathEval("property/bean[@class='com.wi.max.transport.retry.algorithm.ShortcodeRebillAlgorithm']/constructor-arg/map/entry/@key")        
        
        shortCodeAttempts=[]
        
        for e in entries:
            d={}    
            key=e.getContent().strip()
            d[key]=[]
            w=[]    
                                              
            vals=ctx.xpathEval("property/bean[@class='com.wi.max.transport.retry.algorithm.ShortcodeRebillAlgorithm']/constructor-arg/map/entry[@key="+key+"]/list/value")                                   
            
            for value in vals:
                v=value.getContent().strip()                       
                w.append(v.split(",")  )
                
            d[key]=w
                           
            shortCodeAttempts.append(d)

        algorithm=ShortcodeRebillAlgorithm(shortCodeAttempts, lowerPriority)

        return algorithm
    
    def parseEventProducerBeanName(self, ctx, beanName):
        
        
       
        """"
        <property name="retryAlgorithm">
                  <bean class="com.wi.max.transport.retry.algorithm.RerouteAlgorithm">
                       <property name="regexRule">
                         <bean class="com.wi.max.util.RegexRuleSet">
                               <constructor-arg>
                                 <list>
                                     <value>free_shortcode ^$|null|\s+ @NULL@ bill_shortcode_id .* 37 bill_submitted .* true</value> <!-- Default shortcode route (3300) -->
                                        <value>bill_shortcode_id=free_shortcode (.*)\|(.*) $1 bill_submitted .* true</value> 
                                     <value>bill_shortcode_id .* 37 bill_submitted .* true</value> <!-- Default shortcode route (3300) -->        
                                 </list>
                               </constructor-arg>
                         </bean>
                       </property>
                     <property name="eventProducer">
                           <ref bean="messageEventProducer"/> 
                       </property>
                       <property name="markAsBilledIfSubmitted">
                           <value>true</value>
                       </property>
                   </bean>
               </property>
        """
        messageEventProducerNameBean=None       
        
        #try:    
                      
        messageEventProducerNameBean=ctx.xpathEval("//property/bean[@class='"+beanName+"']/property/ref[normalize-space()]")
        #//row[col[@name='POW' and not(normalize-space()) and not(*)]]
          
        for k in messageEventProducerNameBean:
            print k.getContent()                           
                                          
        #except IndexError:
        #    print "Not present"
        #    messageEventProducerNameBean=None   
        
        return messageEventProducerNameBean       
    
    def parseMarkAsBilledIfSubmittedRerouteAlgorithmAlgorithm(self,ctx):
               
        markAsBilledIfSubmitted=False
        
        try:    
                      
            markAsBilledIfSubmitted=ctx.xpathEval("property/bean[@class='com.wi.max.transport.retry.algorithm.RerouteAlgorithm']/property[@name='markAsBilledIfSubmitted']")[0].getContent().strip()                                
                                              
        except IndexError:
           
            markAsBilledIfSubmitted=False   
        
        return markAsBilledIfSubmitted                                                        
                               
    def parseRerouteAlgorithmAlgorithm(self,ctx):                                                   
                        
        vals=ctx.xpathEval("property/bean[@class='com.wi.max.transport.retry.algorithm.RerouteAlgorithm']/property/bean/constructor-arg/list/value")
        
        values=[]
        
        for value in vals:                              
            values.append(value.getContent().strip())     
        
        markAsBilledIfSubmitted=self.parseMarkAsBilledIfSubmittedRerouteAlgorithmAlgorithm(ctx)        
        
        messageEventProducerNameBean=self.parseEventProducerBeanName(ctx, "com.wi.max.transport.retry.algorithm.RerouteAlgorithm")
            
        algorithm=RerouteAlgorithm(values,None,markAsBilledIfSubmitted)

        return algorithm                           
    
    def hasErrorList(self,ctx):
        try: 
        
            ctx.xpathEval("property[@name='errorList']/list/value")[0].getContent().strip()
                                                      
            return True            
                                              
        except IndexError:
            return False
    
    def parseErrorList(self,ctx):
        
        foo=ctx.xpathEval("property[@name='errorList']/list/value")
        errors =[]
        
        for j in foo:
            errors.append(j.getContent().strip())
        
        return errors    

    def printShortcodeOptOutAlgorithmMapping(self,data):
        pass

    def getFileToWrite(self, algorithmName):
         
        fileNameToWrite=algorithmName+".csv"       
                
        return open(fileNameToWrite,"a" if os.path.exists(fileNameToWrite) else "w")
       

    def printRerouteAlgorithmMapping(self, data):
        
        fileToWrite=self.getFileToWrite(RerouteAlgorithm.__class__.__name__)
        
        try:            
        
            fm=data['fileConf']
            le=len(fm.getFileName())-4
            
            for algorithmMapping in fm.getRerouteAlgorithmAlgorithmMappings():
                algorithm=algorithmMapping.getAlgorithm()                    
                errorList=",".join(algorithmMapping.getErrorList())
                for rul in algorithm.getRules():
                    foo="{0},{1},{2},{3},{4},,,,,,,,,,,,,\"{5}\"".format(data['country'],"Carrier",fm.getFileName()[0:le],algorithm.__class__.__name__,rul,errorList)
                    fileToWrite.write(foo)  
                    fileToWrite.write("\n")
                
        except:
            print "Unexpected error:", sys.exc_info()[0]
            raise
        finally:
            fileToWrite.close();

    def printShortcodeRebillAlgorithmMapping(self, data):
        
        fileToWrite=self.getFileToWrite(ShortcodeRebillAlgorithm.__class__.__name__)
        
        try:                            
         
            fm=data['fileConf']
            le=len(fm.getFileName())-4
            
            for algorithmMapping in fm.getShortcodeRebillAlgorithmsMappings():
                algorithm=algorithmMapping.getAlgorithm()                    
                errorList=",".join(algorithmMapping.getErrorList())

                dics=algorithm.getShortCodeAttempts()
                for dic in dics:
                    for key in dic.keys():                                            
                        for value in dic[key]:    
                            foo="{0},{1},{2},{3},,{4},{5},{6},{7},,,,,,,,,\"{8}\"".format(data['country'],"Carrier",fm.getFileName()[0:le],algorithm.__class__.__name__,str(key),value[0],value[1],algorithm.getLowerPriority(),errorList)
                            fileToWrite.write(foo)                                                                                                                                                                                        
                            fileToWrite.write("\n")
                
        except:
            print "Unexpected error:", sys.exc_info()[0]
            raise
        finally:
            fileToWrite.close();

    def printBackoffRetryAlgorithmMapping(self, data):
        
        fileToWrite=self.getFileToWrite(BackoffRetryAlgorithm.__class__.__name__)
        
        try:                                    
            
            fm=data['fileConf']
            le=len(fm.getFileName())-4
            
            for algorithmMapping in fm.getBackoffRetryAlgorithmMappings():
                algorithm=algorithmMapping.getAlgorithm()                    
                errorList=",".join(algorithmMapping.getErrorList())
                foo="{0},{1},{2},{3},,,{4},,{5},{6},{7},,,,,,,\"{8}\"".format(data['country'],"Carrier",fm.getFileName()[0:le],algorithm.__class__.__name__,algorithm.getInterval(),algorithm.getLowerPriority(),algorithm.getMaxRetry(),algorithm.getTimeToLive(),errorList)
                fileToWrite.write(foo)
                fileToWrite.write("\n")
                
        except:
            print "Unexpected error:", sys.exc_info()[0]
            raise
        finally:
            fileToWrite.close();

    def printSimpleRetryAlgorithmMapping(self, data):
        
        fileToWrite=self.getFileToWrite(SimpleRetryAlgorithm.__class__.__name__)
        
        try:                               
            
            fm=data['fileConf']
            le=len(fm.getFileName())-4
            
            for algorithmMapping in fm.getSimpleRetryAlgorithmMappings():
                algorithm=algorithmMapping.getAlgorithm()                    
                errorList=",".join(algorithmMapping.getErrorList())                
                foo="{0},{1},{2},{3},,,{4},,{5},{6},{7},,,,,,,\"{8}\"".format(data['country'],"Carrier",fm.getFileName()[0:le],algorithm.__class__.__name__,algorithm.getInterval(),algorithm.getLowerPriority(),algorithm.getMaxRetry(),algorithm.getTimeToLive(),errorList)                                                                        
                fileToWrite.write(foo)               
                fileToWrite.write("\n")
                
        except:
            print "Unexpected error:", sys.exc_info()[0]
            raise
        finally:
            fileToWrite.close();
    
    def printResult(self):
        
        fileToWrite=self.getFileToWrite(Constants.serverNames[self.currentServerNameIndex]+strftime("%d%m%y"))
  
        try:            
            
            fileToWrite.write("Country,Carrier,Connection,Rebilling type,Rule,Short code,Interval,Short code premium Id,Lower priority,MaxRetry,TimeToLive,Produce event,Queue name,Bulk delivery queue name,Server,Router,Connection factory,Error List")
            fileToWrite.write("\n")
            files=["simpleRetryAlgorithm.txt","shortcodeRebillAlgorithm.txt","backoffRetryAlgorithm.txt","rerouteAlgorithmAlgorithm.txt"]
            concat = ''.join([open(f).read() for f in files])
             
            fileToWrite.write(concat)
             
            for k in files:
                os.remove(k)
                    
        except:
            print "Unexpected error:", sys.exc_info()[0]
            raise
        finally:
            fileToWrite.close();
    
    
    def analyse(self):        
    
        for index, serverName in enumerate(Constants.serverNames):
            
            self.currentServerNameIndex=index
            
            dirsPath=os.path.join(self.root, serverName, Constants.defaultConfDir)
            print dirsPath
            dirs = os.listdir(dirsPath)
            
            data={'country':'','fileConf':''}
            
            for d in dirs:
                
                filesPath=os.path.join(dirsPath,d)                        
                
                files=os.listdir(filesPath)
                
                data['country']=d
                
                for f in files:                            
                                    
                    data['fileConf'] = FileConfiguration.FileConfiguration(f)
                                    
                    filePath=os.path.join(filesPath,f)
                    
                    doc = libxml2.parseFile(filePath)
                    
                    ctx = doc.xpathNewContext()                
                    mappings = ctx.xpathEval("//beans/bean/property[@name='retryMappingList']/list/bean[@class='com.wi.max.transport.retry.RetryMapping']")
     
                    for mapping in mappings:                                    
                               
                        ctx.setContextNode(mapping)  
                        
                        algorithm = None
                        errorList = None
                        
                        if self.isSimpleRetryAlgorithmMapping(ctx):
                            
                            pass#algorithm=self.parseSimpleRetryAlgorithm(ctx)
    
                        elif self.isShortcodeRebillAlgorithmMapping(ctx):
                            
                            pass#algorithm=self.parseShortcodeRebillAlgorithm(ctx)
               
                        elif self.isRerouteAlgorithmMapping(ctx):
                            print filePath
                            self.filePath=filePath
                            algorithm=self.parseRerouteAlgorithmAlgorithm(ctx)                        
                            
                        elif self.isBackoffRetryAlgorithm(ctx):
                        
                            pass#algorithm=self.parseBackoffRetryAlgorithm(ctx)
                            
                        elif self.isShortcodeOptOutAlgorithm(ctx):
                            pass#print f
                        """
                        if algorithm is None:
                            print f
                           
                        if self.hasErrorList(ctx):
                            
                            errorList=self.parseErrorList(ctx)                       
                                  
                        data['fileConf'].addMapping(RetryMapping(algorithm, errorList))                    
                        """               
                    doc.freeDoc()
                    ctx.xpathFreeContext() 
                    """
                    self.printSimpleRetryAlgorithmMapping(data)                 
                    self.printShortcodeRebillAlgorithmMapping(data)                
                    self.printRerouteAlgorithmAlgorithmMapping(data)                
                    self.printBackoffRetryAlgorithmMapping(data)                
                    """           
            #self.printResult()
        
foo=RebillingAnalyser("/home/andres")
foo.analyse()

