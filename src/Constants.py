import os

class Constants(object):
    
    serverNames=["ca1prodmxwimax01","ca1prodmxwimax02","ca1prodmxwimax03"]
    serverIps=["64.151.122.94","64.151.122.67","64.151.122.68"]
    defaultConfDir=os.path.join("opt","wimax","conf")